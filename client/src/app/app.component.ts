import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    iconmap = { // incomplete
        "partly-cloudy-night": "owi-02n",
        "partly-cloudy-day":   "owi-02d",
        "clear-day":           "owi-01d"
    };

    url  = '/api/';
    data = null;

    constructor(private http: HttpClient) {}

    ngOnInit(): void {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(pos => {
                this.http.get(
                    this.url + pos.coords.latitude + ',' + pos.coords.longitude
                ).subscribe( data => {
                    this.data = data;
                },
                err => {
                    console.log('Error occured.', err);
                });

            });
        }

    }

}
