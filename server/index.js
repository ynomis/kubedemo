const app = require('express')();
const request = require('request');
const fs = require('fs');

const darksky_key = '5691363605d425e8ade28a51e3c7ec15';
const url = 'https://api.darksky.net/forecast/' + darksky_key + '/';

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/:coords', (req, res) => {

    console.log('[INFO] ' + req.protocol + '://' + req.get('host') + req.originalUrl);

    let coordination = req.params.coords;

    request(url + coordination, function(err, resp, body) {
        if (!err && 200 === resp.statusCode) {
            res.send(body);
        } else {
            console.log('[DEBUG] URL tried, ', resp.statusCode, url + coordination);
            res.send('Something is wrong, check your code.');
        }
    });

});

app.listen(3000, () => console.log('HTTP server running...'));
